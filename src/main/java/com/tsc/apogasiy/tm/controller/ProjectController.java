package main.java.com.tsc.apogasiy.tm.controller;

import main.java.com.tsc.apogasiy.tm.api.controller.IProjectController;
import main.java.com.tsc.apogasiy.tm.api.service.IProjectService;
import main.java.com.tsc.apogasiy.tm.api.service.IProjectTaskService;
import main.java.com.tsc.apogasiy.tm.enumerated.Sort;
import main.java.com.tsc.apogasiy.tm.enumerated.Status;
import main.java.com.tsc.apogasiy.tm.exception.empty.EmptyProjectList;
import main.java.com.tsc.apogasiy.tm.exception.entity.ProjectNotFoundException;
import main.java.com.tsc.apogasiy.tm.model.Project;
import main.java.com.tsc.apogasiy.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class ProjectController implements IProjectController {

    private final IProjectService projectService;
    private final IProjectTaskService projectTaskService;

    public ProjectController(final IProjectService projectService, final IProjectTaskService projectTaskService) {
        this.projectService = projectService;
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void showProjects() {
        // Завершаем, если нет проектов
        if (projectService.isEmpty())
            throw new EmptyProjectList();

        final List<Project> projects;
        System.out.println("[ENTER SORT]");
        System.out.println(Arrays.toString(Sort.values()));
        final String sort = TerminalUtil.nextLine();

        if (sort == null || sort.isEmpty() || !Sort.isValid(sort))
            projects = projectService.findAll();
        else {
            Sort sortType = Sort.valueOf(sort);
            projects = projectService.findAll(sortType.getComparator());
        }

        System.out.println("[LIST PROJECTS]");

        for (Project project : projects)
            System.out.println(project);
        System.out.println("[OK]");
    }

    public void showProject(Project project) {
        if (project == null)
            return;
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("Status: " + project.getStatus().getDisplayName());
    }

    @Override
    public void clearProjects() {
        System.out.println("[CLEAR PROJECTS]");
        projectService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION]");
        final String description = TerminalUtil.nextLine();
        projectService.create(name, description);
        System.out.println("[OK]");
    }

    @Override
    public void showById() {
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findById(id);
        if (project == null)
            throw new ProjectNotFoundException();
        showProject(project);
    }

    @Override
    public void showByName() {
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.findByName(name);
        if (project == null)
            throw new ProjectNotFoundException();
        showProject(project);
    }

    @Override
    public void showByIndex() {
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findByIndex(index);
        if (project == null)
            throw new ProjectNotFoundException();
        showProject(project);
    }

    @Override
    public void updateByIndex() {
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        if (!projectService.existsByIndex(index))
            throw new ProjectNotFoundException();
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION]");
        final String description = TerminalUtil.nextLine();
        projectService.updateByIndex(index, name, description);
        System.out.println("[OK]");
    }

    @Override
    public void updateById() {
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        if (!projectService.existsById(id))
            throw new ProjectNotFoundException();
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION]");
        final String description = TerminalUtil.nextLine();
        projectService.updateById(id, name, description);
        System.out.println("[OK]");
    }

    @Override
    public void startById() {
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.startById(id);
        if (project == null)
            throw new ProjectNotFoundException();
    }

    @Override
    public void startByIndex() {
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.startByIndex(index);
        if (project == null)
            throw new ProjectNotFoundException();
    }

    @Override
    public void startByName() {
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.startByName(name);
        if (project == null)
            throw new ProjectNotFoundException();
    }

    @Override
    public void finishById() {
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.finishById(id);
        if (project == null)
            throw new ProjectNotFoundException();
    }

    @Override
    public void finishByIndex() {
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.finishByIndex(index);
        if (project == null)
            throw new ProjectNotFoundException();
    }

    @Override
    public void finishByName() {
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.finishByName(name);
        if (project == null)
            throw new ProjectNotFoundException();
    }

    @Override
    public void changeStatusById() {
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        System.out.println("[ENTER STATUS]");
        System.out.println((Arrays.toString(Status.values())));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusValue);
        final Project project = projectService.changeStatusById(id, status);
        if (project == null)
            throw new ProjectNotFoundException();
    }

    @Override
    public void changeStatusByIndex() {
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("[ENTER STATUS]");
        System.out.println((Arrays.toString(Status.values())));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusValue);
        final Project project = projectService.changeStatusByIndex(index, status);
        if (project == null)
            throw new ProjectNotFoundException();
    }

    @Override
    public void changeStatusByName() {
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER STATUS]");
        System.out.println((Arrays.toString(Status.values())));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusValue);
        final Project project = projectService.changeStatusByName(name, status);
        if (project == null)
            throw new ProjectNotFoundException();
    }

}

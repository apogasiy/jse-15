package main.java.com.tsc.apogasiy.tm.exception.system;

import main.java.com.tsc.apogasiy.tm.exception.AbstractException;

public class IndexIncorrectException extends AbstractException {

    public IndexIncorrectException() {
        super("Error! Index is incorrect!");
    }

}

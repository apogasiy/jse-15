package main.java.com.tsc.apogasiy.tm.api.entity;

import java.util.Date;

public interface IHasCreated {

    Date getCreated();

    void setCreated(Date created);

}
